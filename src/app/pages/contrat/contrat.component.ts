import { ConfirmationService } from 'primeng/api';
import { Router, ActivatedRoute } from '@angular/router';
import { ContratService } from './contrat.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IContrat } from '../../shared/models/contrat.model';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ITEMS_PER_PAGE } from '../../utils/app.constants';

@Component({
  selector: 'ngx-create-contrat',
  templateUrl: 'contrat.component.html'
})


export class ContratComponent implements OnInit {


  contratsList: IContrat[];
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;


  constructor(private contratService: ContratService,
    private activatedRouter: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRouter.data.subscribe(data => {
    this.page = data.pagingParams.page;
    this.previousPage = data.pagingParams.page;
    this.reverse = data.pagingParams.ascending;
    this.predicate = data.pagingParams.predicate;

    });


  }

  ngOnInit(): void {

  }

  loadAll(event?: any) {
    this.page--;
    if (event) {
      if (event.page) {
        this.page = event.page;
      }
    }

    this.contratService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IContrat[]>) => {
          let content = res.headers.get('Link');
          this.paginateContrat(res.body, res.headers);
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );

  }

  protected paginateContrat(data: IContrat[], headers: HttpHeaders) {
    this.links = this.parseLink(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.contratsList = data;
  }

  protected onError(errorMessage: string) {
    alert(errorMessage);
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  parseLink(header: string): any {
    if (header.length === 0) {
      throw new Error('input must not be of zero length');
    }


    const parts: string[] = header.split(',');
    const links: any = {};


    parts.forEach(p => {
      const section: string[] = p.split(';');

      if (section.length !== 2) {
        throw new Error('section could not be split on ";"');
      }

      const url: string = section[0].replace(/<(.*)>/, '$1').trim();
      const queryString: any = {};

      url.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'), ($0, $1, $2, $3) => (queryString[$1] = $3));

      let page: any = queryString.page;

      if (typeof page === 'string') {
        page = parseInt(page, 10);
      }

      const name: string = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = page;
    });
    return links;
  }

  create() {
    this.router.navigate(['new'], { relativeTo: this.activatedRouter });
  }

  edit(id: number) {
    this.router.navigate([id, 'edit'], { relativeTo: this.activatedRouter });
  }

  delete(id: number) {
    this.confirmationService.confirm({
      message: "voulez vous continuez",
      accept: () => { this.deleteEntity(id) },
      reject: () => { this.loadAll() },
    });
  }

  deleteEntity(id: number) {
    this.contratService.delete(id)
      .subscribe(res => {
        this.loadAll();
      });
  }

}
