import { IUtilisateur } from './../../shared/models/utilisateur.model';
import { UtilisateurService } from './../utilisateur/utilisateur.service';
import { MessageService } from 'primeng/api';
import { ContratService } from './contrat.service';
import { Contrat } from './../../shared/models/contrat.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IContrat } from '../../shared/models/contrat.model';
import { Utilisateur } from '../../shared/models/utilisateur.model';

@Component({
  selector: 'ngx-create-or-edit-contrat',
  templateUrl: 'create-or-edit-contrat.component.html'
})

export class CreateOrEditContratComponent implements OnInit {

  model: IContrat;
  routeData: any;
  title: string;
  utilisateurs: Utilisateur[];
  selectUtilisateur: IUtilisateur;


  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private contratService: ContratService,
    private messageService: MessageService,
    private utilisateurService: UtilisateurService) {

    this.model = new Contrat();

  }


  ngOnInit(): void {
    this.routeData = this.activatedRoute.data.subscribe(data => {
      if (data.contrat) {
        this.model = data.contrat;
      }
    });

    this.loadClient();
  }

  loadClient() {
    this.utilisateurService.getAsList().subscribe(resp => {
      this.utilisateurs = resp.body;
      if (this.model.id) {
        this.selectUtilisateur = this.utilisateurs.find(e => e.id === this.model.utilisateurId);
      }
    })

  }


  save() {
    this.model.utilisateurId = this.selectUtilisateur.id;
    if (this.model.id) {
      this.contratService.update(this.model).subscribe(
        resp => {
          this.notify('success', 'success', 'Contrat bien modifié');
          this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
        });
    } else {
      this.contratService.create(this.model).subscribe(resp => {
        this.notify('success', 'success', 'Contrat bien enregistrer');
        this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
      });
    }
  }

  checkResponse(response: any) {
    if (response.status === 201) {
      this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'Contrat ajouté' });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Success Message', detail: 'not submited' });
    }
  }

  notify(severity: string, title: string, message: string) {
    this.messageService.add({ severity: severity, summary: title, detail: message });
  }

  backToList() {
    this.router.navigate(['../..'], { relativeTo: this.activatedRoute });
  }

}
