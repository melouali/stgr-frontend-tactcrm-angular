import { FactureService } from './facture.service';
import { Component, OnInit } from '@angular/core';
import { IFacture } from '../../shared/models/facture.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ITEMS_PER_PAGE } from '../../utils/app.constants';


@Component({
  selector: 'ngx-facture',
  templateUrl: './facture.component.html',
  styleUrls: []
})
export class FactureComponent implements OnInit {

  factures: IFacture[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;


  constructor(
    private factureService: FactureService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {
    this.itemsPerPage = 4;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  ngOnInit(): void {
  }

  loadAll(event?: any) {
    this.page--;
    if (event) {
      if (event.page) {
        this.page = event.page;
      }
    }
    this.factureService.query({
      page: this.page,
      size: this.itemsPerPage,
      sort: this.sort()
    }).subscribe(
      (res: HttpResponse<IFacture[]>) => {
        let content = res.headers.get('Link');
        this.paginateConsultation(res.body, res.headers);
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  protected paginateConsultation(data: IFacture[], headers: HttpHeaders) {
    this.links = this.parseLink(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.factures = data;
  }

  protected onError(errorMessage: string) {
    alert(errorMessage);
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  create() {
    this.router.navigate(['new'], { relativeTo: this.activatedRoute });
  }

  edit(id: number) {
    this.router.navigate([id, 'edit'], { relativeTo: this.activatedRoute });
  }

  parseLink(header: string): any {
    if (header.length === 0) {
      throw new Error('input must not be of zero length');
    }
    const parts: string[] = header.split(',');
    const links: any = {};

    parts.forEach(p => {
      const section: string[] = p.split(';');
      if (section.length !== 2) {
        throw new Error('section could not be split on ";"');
      }
      const url: string = section[0].replace(/<(.*)>/, '$1').trim();
      const queryString: any = {};
      url.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'), ($0, $1, $2, $3) => (queryString[$1] = $3));
      let page: any = queryString.page;
      if (typeof page === 'string') {
        page = parseInt(page, 10);
      }
      const name: string = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = page;
    });
    return links;
  }

  onDeleteConfirm(id: number): void {
    this.factureService.delete(id).subscribe(response => {
      this.notify('success', 'success', 'Facture supprimée avec succés');
      this.loadAll();
    });

  }
  notify(severity: string, title: string, message: string) {
    this.messageService.add({ severity: severity, summary: title, detail: message });
  }

  delete(id: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.onDeleteConfirm(id);
      },
      reject: () => {
        this.loadAll();
      }
    });
  }
}
