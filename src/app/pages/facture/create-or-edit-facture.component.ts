import { IFactureResult, FactureCalculationService } from './facture-calcule.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { DetailsFactureService } from './details-facture.service';
import { ArticlesService } from './../article/article.service';
import { TaxeService } from './../taxe/taxe.service';
import { DeviseService } from './../devise/devise.service';
import { TypePaiementService } from './../type-paiement/type-paiement.service';
import { UtilisateurService } from './../utilisateur/utilisateur.service';
import { FactureService } from './facture.service';
import { DetailsFacture, IDetailsFacture } from './../../shared/models/details-facture.model';
import { Article, IArticle } from './../../shared/models/article.model';
import { ITaxe } from './../../shared/models/taxe.model';
import { IDevise } from './../../shared/models/devise.model';
import { ITypePaiement } from './../../shared/models/type-paiement.model';
import { IUtilisateur } from './../../shared/models/utilisateur.model';
import { Component, OnInit } from '@angular/core';
import { Facture, IFacture } from '../../shared/models/facture.model';

@Component({
  selector: 'ngx-create-or-edit-facture',
  templateUrl: './create-or-edit-facture.component.html'
})
export class CreateOrEditFactureComponent implements OnInit {

  model: IFacture;
  clients: IUtilisateur[];
  selectedClient: IUtilisateur;
  typesPaiement: ITypePaiement[];
  selectedTypePaiement: ITypePaiement;
  devises: IDevise[];
  selectedDevise: IDevise;
  taxes: ITaxe[];
  selectedTaxe: ITaxe;
  selectedNumeroFacture: string;
  selectedDateFacturation: Date;

  articleModel: Article;
  articles: IArticle[];
  selectedArticle: IArticle;
  selectedQuantite: number;
  detailsFactureModel: DetailsFacture;
  detailsFactureList: IDetailsFacture[];

  routeData: any;
  coutTotal: IFactureResult;

  constructor(
    private factureService: FactureService,
    private utilisateurService: UtilisateurService,
    private typePaiementService: TypePaiementService,
    private deviseService: DeviseService,
    private taxeService: TaxeService,
    private articlesService: ArticlesService,
    private detailsFactureService: DetailsFactureService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private calculService: FactureCalculationService
  ) {
    this.model = new Facture();
    this.articleModel = new Article();
    this.detailsFactureModel = new DetailsFacture();
    this.detailsFactureList = [];
  }

  ngOnInit(): void {
    this.routeData = this.activatedRoute.data.subscribe(
      data => {
        if (data.facture) {
          this.model = data.facture;
          this.loadDetailsFacture();
        }
      }
    )
    this.loadClients();
    this.loadTypesPaiements();
    this.loadDevises();
    this.loadTaxes();
    this.loadArticles();
    if (this.model.id) {
      this.selectedDateFacturation = this.model.dateFacturation;
      this.selectedNumeroFacture = this.model.numeroFacture;
    }
  }

  loadClients() {
    this.utilisateurService.getAsList().subscribe(
      res => {
        this.clients = res.body;
        if (this.model.id) {
          this.selectedClient = this.clients.find(item => item.id === this.model.utilisateurId);
        }
      }
    );
  }

  loadTypesPaiements() {
    this.typePaiementService.getAsList().subscribe(
      res => {
        this.typesPaiement = res.body;
        if (this.model.id) {
          this.selectedTypePaiement = this.typesPaiement.find(item => item.id === this.model.typePaiementId);
        }
      }
    );
  }

  loadDevises() {
    this.deviseService.getAsList().subscribe(
      res => {
        this.devises = res.body;
        if (this.model.id) {
          this.selectedDevise = this.devises.find(item => item.id === this.model.deviseId);
        }
      }
    );
  }

  loadTaxes() {
    this.taxeService.getAsList().subscribe(
      res => {
        this.taxes = res.body;
        if (this.model.id) {
          this.selectedTaxe = this.taxes.find(item => item.id === this.model.taxeId);
        }
      }
    );
  }

  loadArticles() {
    this.articlesService.getAsList().subscribe(
      res => {
        this.articles = res.body;
      }
    );
  }

  loadDetailsFacture() {
    if (this.model.id) {
      this.detailsFactureService.getDetailsFactureByFactureId(this.model.id).subscribe(
        res => {
          this.detailsFactureList = res.body;
          if (this.detailsFactureList && this.detailsFactureList.length > 0) {
            this.coutTotal = this.calculCoutTotal(this.detailsFactureList);
          }
        }
      );
    }
  }

  saveFacture(isRedirected?: boolean) {
    if (!this.selectedClient || !this.selectedTypePaiement ||
      !this.selectedNumeroFacture || !this.selectedDateFacturation ||
      !this.selectedDevise || !this.selectedTaxe) {
      this.notify('error', 'error', 'Merci de remplir tous les éléments de la facture !');

    } else {
      if (this.detailsFactureList.length <= 0 && isRedirected != false) {
        this.notify('error', 'error', 'Merci d\'ajouter au moins un article et sa quantité !');

      } else {
        this.model.utilisateurId = this.selectedClient.id;
        this.model.typePaiementId = this.selectedTypePaiement.id;
        this.model.deviseId = this.selectedDevise.id;
        this.model.taxeId = this.selectedTaxe.id;
        this.model.dateFacturation = this.selectedDateFacturation;
        this.model.numeroFacture = this.selectedNumeroFacture;

        if (!this.model.id) {
          this.factureService.create(this.model).subscribe(
            res => {
              this.model = res.body;
              this.saveDetailsFacture();
              this.notify('success', 'success', 'Facture enregistrée avec succès');
            }
          );

        } else {
          this.factureService.update(this.model).subscribe(
            res => {
              this.model = res.body;
              this.notify('success', 'success', 'Facture modifiée avec succès');
            }
          );
        } if (isRedirected != false) {
          this.router.navigateByUrl('/pages/factures');
        }
      }
    }
  }

  notify(severity: string, title: string, message: string) {
    this.messageService.add({ severity: severity, summary: title, detail: message });
  }

  saveDetailsFacture() {
    if (!this.selectedArticle || this.selectedQuantite <= 0 || !this.selectedQuantite) {
      this.notify('error', 'error', 'Merci de choisir un article et une quantité positive !');

    } else {
      if (this.model.id) {
        this.detailsFactureModel.factureId = this.model.id;
        this.detailsFactureModel.articleId = this.selectedArticle.id;
        this.detailsFactureModel.prixFactureHt = this.selectedArticle.prixVente;
        this.detailsFactureModel.quantite = this.selectedQuantite;
        this.detailsFactureService.create(this.detailsFactureModel).subscribe(
          () => {
            this.loadDetailsFacture();
          }
        )

      } else {
        this.saveFacture(false);
      }
    }
  }

  deleteDetailsFacture(id: number) {
    this.confirmationService.confirm(
      {
        message: "Etes-vous sûr(e) de vouloir supprimer cet élément ?",
        accept: () => { this.confirmDelete(id); },
        reject: () => { this.loadDetailsFacture(); }
      }
    );
  }

  confirmDelete(id: number) {
    this.detailsFactureService.delete(id).subscribe(
      () => {
        this.loadDetailsFacture();
      }
    );
  }

  backToList() {
    this.router.navigateByUrl('/pages/factures');
  }

  calculCoutTotal(details: IDetailsFacture[]): IFactureResult {
    return this.calculService.calculate(details);
  }
}
