import { FactureComponent } from './facture.component';
import { NgModule } from '@angular/core';
import { FactureRoutingModule } from './facture-routing.module';
import { TableModule } from 'primeng/table';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UtilsModule } from '../../shared/utils.module';
import { DropdownModule } from 'primeng/dropdown';
import { FactureClientComponent } from './client/facture-client.component';
import { ViewFactureComponent } from './client/view-facture.component';
import { PaginatorModule } from 'primeng/paginator';
import { CreateOrEditFactureComponent } from './create-or-edit-facture.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    FactureRoutingModule,
    TableModule,
    PaginatorModule,
    UtilsModule,
    DropdownModule,
  ],
  exports: [],
  declarations: [
    FactureClientComponent,
    ViewFactureComponent,
    CreateOrEditFactureComponent,
    FactureComponent],
  providers: [],
})
export class FactureModule { }
