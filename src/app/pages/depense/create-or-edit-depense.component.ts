import { IDepense, Depense } from './../../shared/models/depense.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DepenseService } from './depense.service';
import { MessageService } from 'primeng/api';
import { TypePaiementService } from '../type-paiement/type-paiement.service';
import { TypeDepenseService } from '../type-depense/type-depense.service';
import { DeviseService } from '../devise/devise.service';
import { ITypeDepense } from '../../shared/models/type-depense.model';
import { ITypePaiement } from '../../shared/models/type-paiement.model';
import { IDevise } from '../../shared/models/devise.model';
@Component({
  selector: 'ngx-create-or-edit-depense',
  templateUrl: 'create-or-edit-depense.component.html',
})
export class CreateOrEditDepenseComponent implements OnInit {


  model: IDepense;
  routeData: any;

  typeDepenses: ITypeDepense[];
  typePaiements: ITypePaiement[];
  devises: IDevise[];
  selectedTypeDepense:ITypeDepense;
  selectedTypePaiement:ITypePaiement;
  selectedDevise:IDevise;
  title: string;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private depenseService: DepenseService,
    private typePaiementService: TypePaiementService,
    private typeDepenseService: TypeDepenseService,
    private deviseService: DeviseService
  ) {
    this.model = new Depense();
  }

  ngOnInit() { //cycle de vie appelé par Angular pour indiquer que la création du composant est terminé

    this.routeData = this.activatedRoute.data.subscribe(data => {
        this.title =  data.title;
        if(data.depense){
            this.model = data.depense;
        }
    });
    this.loadTypeDepense();
    this.loadTypePaiement();
    this.loadDevise();
}


  save(){
    this.model.typeDepenseId = this.selectedTypeDepense.id;
    this.model.typePaiementId = this.selectedTypePaiement.id;
    this.model.deviseId = this.selectedDevise.id;
    if(this.model.id){
        this.depenseService.update(this.model).subscribe(res=>{
            this.notify('success', 'success', 'La Depense a bien été modifiée');
            this.router.navigate(['../..'], {relativeTo: this.activatedRoute});
        });
    }else{
        this.depenseService.create(this.model)
        .subscribe(res=>{
            this.notify('success', 'success', 'La Depense a bien été enrgistrée');
            this.router.navigate(['..'], {relativeTo: this.activatedRoute});
        });
    }
}

  checkResponse(response: any) {
    if (response.status === 201) {
      this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'La dépense a bien été ajouté ' });//Alert box succés de l'opératoion
    } else {
      this.messageService.add({ severity: 'error', summary: 'Success Message', detail: 'not submited' });//Alert box error de l'opératoion
    }
  }

  notify(severity: string, title: string, message: string) {
    this.messageService.add({ severity: severity, summary: title, detail: message });
  }
  backToList(){
    if (this.model.id) {
    this.router.navigate(['../..'], {relativeTo: this.activatedRoute});
    }else{
      this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }
}

loadTypePaiement(){
  this.typePaiementService.getAsList()
  .subscribe(res=>{
      this.typePaiements = res.body;
      if(this.model.id){
          this.selectedTypePaiement = this.typePaiements.find(e=>e.id===this.model.typePaiementId);
      }
  });
}
loadTypeDepense(){
  this.typeDepenseService.getAsList()
  .subscribe(res=>{
      this.typeDepenses = res.body;
      if(this.model.id){///edit
          this.selectedTypeDepense = this.typeDepenses.find(e=>e.id===this.model.typeDepenseId);
      }
  });
}
loadDevise(){
  this.deviseService.getAsList()
  .subscribe(res=>{
      this.devises = res.body;
      if(this.model.id){
          this.selectedDevise = this.devises.find(e=>e.id===this.model.deviseId);
      }
  });
}





}
